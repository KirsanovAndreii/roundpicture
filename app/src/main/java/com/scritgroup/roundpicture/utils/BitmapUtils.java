package com.scritgroup.roundpicture.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class BitmapUtils {


    public static Bitmap getBitmapPhotoCamera(Context context, String fileName) {
// разворот при необходимости для корректного отображения
        int rotation = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(fileName);
            // String orientString = exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotation = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotation = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotation = 270;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, bounds);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap photoTransform = BitmapFactory.decodeFile(fileName, opts);

        // переворачиваем, если положение фото не корректное
        if (rotation != 0) {
            Matrix matrix = new Matrix();
            matrix.setRotate(rotation, (float) photoTransform.getWidth() / 2, (float) photoTransform.getHeight() / 2);
            photoTransform = Bitmap.createBitmap(photoTransform, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        }

        // уменьшаем, исли фото большого размера с back камеры до 1080 (меньшая сторона)
        int minSide = (photoTransform.getWidth() < photoTransform.getHeight()) ? photoTransform.getWidth() : photoTransform.getHeight();

        if (minSide > 1080) {
            Matrix matrix = new Matrix();
            float scale = 1080f / minSide;
            matrix.postScale(scale, scale);
            photoTransform = Bitmap.createBitmap(photoTransform, 0, 0, photoTransform.getWidth(),
                    photoTransform.getHeight(), matrix, true);
            //  Log.d("___scal______  ",photoTransform.getWidth()+"   "+photoTransform.getHeight());
        }
        return photoTransform;
    }


    public static void saveBitmapPhotoFile(Bitmap bitmap, File fileName) {

        File file = fileName;
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
