package com.scritgroup.roundpicture.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.scritgroup.roundpicture.R;
import com.scritgroup.roundpicture.utils.BitmapUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;


public class MainActivity extends AppCompatActivity {

    private TextView buttonTakePhoto;
    private TextView buttonCancel;
    private TextView buttonDone;

    private ImageView croppedImage;
    private CropImageView cropImageView;
    private Bitmap photoBitmapOriginal;

    private String fileFullName; // путь к файлу
    // или
    private File photoFile;

    private int sizeResultPhoto = 500; // размер WxH круглого фото

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonTakePhoto = findViewById(R.id.button_takephoto);
        buttonCancel = findViewById(R.id.button_cancel);
        buttonDone = findViewById(R.id.button_done);
        croppedImage = findViewById(R.id.image_croped);
        cropImageView = findViewById(R.id.crop_image_view);


        //----------------исходная фотография
       // fileFullName = "/sdcard/Android/data/com.scritgroup.roundpicture/files/Pictures/20200608_174945.jpg";
        fileFullName = "/sdcard/Android/data/com.scritgroup.roundpicture/files/Pictures/20200608_152905.jpg";
        photoFile = new File(fileFullName);
        //--------------
    }


    @Override
    protected void onResume() {
        super.onResume();

        buttonTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        getBitmapPhoto();
                    } else {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }
                } else {
                    getBitmapPhoto();
                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Bitmap croppedBitmap = CropImage.toOvalBitmap(Bitmap.createScaledBitmap(cropImageView.getCroppedImage(),
                        sizeResultPhoto, sizeResultPhoto, true));

                croppedImage.setImageBitmap(croppedBitmap);
                cropImageView.setVisibility(View.INVISIBLE);

                // формируем имя обрезанного файла
                String cropFileName = "crop_" + photoFile.getName().split("\\.")[0] + ".png";
                final File cropFile = new File(photoFile.getParentFile(), cropFileName);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        BitmapUtils.saveBitmapPhotoFile(croppedBitmap, cropFile);
                    }
                }).start();

            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }


    private void getBitmapPhoto() {
        croppedImage.setImageBitmap(null);
        cropImageView.setVisibility(View.VISIBLE);

        photoBitmapOriginal = BitmapUtils.getBitmapPhotoCamera(MainActivity.this, photoFile.getAbsolutePath());
        cropImageView.setImageBitmap(photoBitmapOriginal);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("photoFile", photoFile.getAbsolutePath());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        photoFile = new File(savedInstanceState.getString("photoFile"));
        getBitmapPhoto();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (photoBitmapOriginal != null && !photoBitmapOriginal.isRecycled()) photoBitmapOriginal.recycle();
        photoBitmapOriginal = null;
    }
}
